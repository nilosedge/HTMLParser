/*
Definitive Guide to Swing for Java 2, Second Edition
By John Zukowski	
ISBN: 1-893115-78-X
Publisher: APress
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.ElementIterator;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;

public class AllStateLeatherUpdatePricing {
	
	private static HTMLDocument htmlDoc;
	private static String newName = "";
	private Connection connect = null;
	
	private boolean insertSQL = false;

	public static void main(String args[]) throws Exception {
		new AllStateLeatherUpdatePricing();
	}
	
	public AllStateLeatherUpdatePricing() throws IOException, BadLocationException {

		// Load HTML file synchronously
		//URL url = new URL("http://www.allstateleather.com/sitemap.jsp");
		//URLConnection connection = url.openConnection();
		//InputStream is = connection.getInputStream();
		//InputStreamReader isr = new InputStreamReader(is);
		//BufferedReader br = new BufferedReader(isr);
		
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String connectString = "jdbc:mysql://mysqlhost/substruct?user=root&password=";
			connect = DriverManager.getConnection(connectString);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		BufferedReader read = new BufferedReader(new FileReader("price.html"));
	
		HTMLEditorKit htmlKit = new HTMLEditorKit();
		htmlDoc = (HTMLDocument) htmlKit.createDefaultDocument();
		HTMLEditorKit.Parser parser = new ParserDelegator();
		HTMLEditorKit.ParserCallback callback = htmlDoc.getReader(0);
		parser.parse(read, callback, true);
	
		// Parse
		ElementIterator iterator = new ElementIterator(htmlDoc);
		Element element;
		while ((element = iterator.next()) != null) {
			AttributeSet attributes = element.getAttributes();
			Object name = attributes.getAttribute(StyleConstants.NameAttribute);
			
	
			if ((name instanceof HTML.Tag) && (name == HTML.Tag.TABLE)) {
				// Build up content text as it may be within multiple elements
				//StringBuffer text = new StringBuffer();
				int count = element.getElementCount();
				
				if(count > 100) {
					System.out.println("-->" + count + "<--");
					for (int i = 0; i < count; i++) {
						Element tr = element.getElement(i);
						parseTr(tr);
					}
				}
	
			}
		}
		System.exit(0);
	}

	
	private void parseTr(Element tr) throws BadLocationException {
		
		int count = tr.getElementCount();
		//System.out.println(count);
		if(count == 5) {
			String data1 = "";
			String data3 = "";
			for (int i = 0; i < count; i++) {
				Element td = tr.getElement(i);
				String data = parseTd(td, i);
				if(!data.equals("") && !data.equalsIgnoreCase("sizes:") && !data.equals("0") && !data.equals("One size fits most")) {
					if(i == 1) data1 = data;
					if(i == 3) data3 = data;
					//System.out.println("NewName: " + newName + " Data" + i + ": " + data);
				}
				//System.out.println("I: " + i + " Data:" + data + ":");
			}
			if(!data1.equals("") && !data3.equals("0") && !data3.equals("") && !data3.equals("0.0")) {
				if(newName.equals(data1)) {
					if(exists(newName)) {
						update(newName, data3);
					}
				} else {
					if(exists(newName + " " + data1)) {
						update(newName + " " + data1, data3);
					}
				}
			}
			//System.out.println();
		}
		
	}
	
	private boolean exists(String code) {
		boolean ret = false;
		try {
			Statement s = connect.createStatement();
			ResultSet r = s.executeQuery("select * from items where code=\"" + code + "\"");
			
			r.last();
			int rowCount = r.getRow();
			r.close();
			s.close();
			ret = rowCount > 0;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(ret);
		return ret;
	}
	
	private void update(String code, String price) {
		try {
			if(insertSQL) connect.createStatement().executeUpdate("update items set price=" + price + " where code = \"" + code + "\"");
			System.out.println("update items set price=" + price + " where code = \"" + code + "\";");
		} catch (Exception e) {
			System.out.println("Did not run query: update items set price=" + price + " where code = \"" + code + "\";");
			e.printStackTrace();
		}
		
	}

	private static String parseTd(Element td, int index) throws BadLocationException {
		//System.out.println(td.getElementCount());
		
		String data = "";
		
		if(td.getElement(0).getName().equals("p-implied")) {
			if(index == 1) {
				data = parseTd2(td.getElement(0));
				
				Pattern p = Pattern.compile("AL\\s*\\d{4}");
				Matcher m = p.matcher(data);
				
				if(m.find()) {
					data = data.substring(m.start(), m.end()).replaceAll(" ", "");
					//System.out.println("New Name: " + data);
					newName = data;
				} else {
					data = "";
				}
			} else if(index == 3) {
				//data = parseTd2(td.getElement(0));
				Double d = parsePrice(td.getElement(0));
				//System.out.println("Data2: " + d);
				data = d.toString();
			}
			

			
		} else if(td.getElement(0).getName().equals("table")) {
			data = parseTable(td.getElement(0));
			
		} else {
			//System.out.println(td.getName());
		}
		if(!data.equals("") && !data.equalsIgnoreCase("sizes:") && !data.equalsIgnoreCase("sizes")) {
			//System.out.println("Data: " + data);
			return data;
		} else {
			return "";
		}
		
	}


	private static String parseTd2(Element element) throws BadLocationException {
		if(element.getElement(0).getName().equals("content")) {
			return parseContent(element.getElement(0));
		}
		return "";
	}

	private static double parsePrice(Element element) throws BadLocationException {
		if(element.getElement(0).getName().equals("content")) {
			String data = parseContent(element.getElement(0));
			//System.out.println("Data3: " + data);
			Pattern p = Pattern.compile("\\$\\s*\\d*\\.\\d\\d");
			Matcher m = p.matcher(data);
			
			if(m.find()) {
				data = data.substring(m.start(), m.end()).replaceAll(" ", "");
				data = data.replaceAll(" ", "");
				data = data.replaceAll("\\$", "");
				//System.out.println("Data3: " + data);
				double price = new Double(data);
				//System.out.println("Price: " + (((int)(price * 1.8)) + .99));
				return (((int)(price * 1.8)) + .99);
			} else {
				return 0;
			}
		}
		return 0;
	}

	private static String parseContent(Element element) throws BadLocationException {
		String line = "";
		if(element.isLeaf()) {
			if (element.getAttributes().getAttribute(StyleConstants.NameAttribute) == HTML.Tag.CONTENT) {
				int startOffset = element.getStartOffset();
				int endOffset = element.getEndOffset();
				int length = endOffset - startOffset;
				//String test = "Out Of Stock";
				line = htmlDoc.getText(startOffset, length);
			}
		}
		return line;
	}
	


	private static String parseTable(Element element) throws BadLocationException {
		//System.out.println("// Parse Table");
		Element content = element.getElement(0).getElement(0).getElement(0).getElement(0);
		//System.out.println(content.getName());
		return parseContent(content);
		
	}
}
