/*
Definitive Guide to Swing for Java 2, Second Edition
By John Zukowski	
ISBN: 1-893115-78-X
Publisher: APress
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.ElementIterator;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;

public class AllStateLeatherUpdateInventory {
	
	private HTMLDocument htmlDoc;
	//private static String newName = "";
	//private FileOutputStream fos;
	//private PrintWriter out;
	private Connection connect = null;
	
	private boolean insertSQL = false;

	public static void main(String args[]) throws Exception {
		new AllStateLeatherUpdateInventory();
	}
	
	public AllStateLeatherUpdateInventory() throws IOException, BadLocationException {
		
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String connectString = "jdbc:mysql://mysqlhost/substruct?user=root&password=";
			connect = DriverManager.getConnection(connectString);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		// Load HTML file synchronously
		//URL url = new URL("http://www.allstateleather.com/sitemap.jsp");
		//URLConnection connection = url.openConnection();
		//InputStream is = connection.getInputStream();
		//InputStreamReader isr = new InputStreamReader(is);
		//BufferedReader br = new BufferedReader(isr);
		
		BufferedReader read = new BufferedReader(new FileReader("inv.html"));
		//fos =  new FileOutputStream(new File("output.sql"));
		//out = new PrintWriter(fos);
	
		HTMLEditorKit htmlKit = new HTMLEditorKit();
		htmlDoc = (HTMLDocument) htmlKit.createDefaultDocument();
		HTMLEditorKit.Parser parser = new ParserDelegator();
		HTMLEditorKit.ParserCallback callback = htmlDoc.getReader(0);
		parser.parse(read, callback, true);
	
		// Parse
		ElementIterator iterator = new ElementIterator(htmlDoc);
		Element element;
		while ((element = iterator.next()) != null) {
			AttributeSet attributes = element.getAttributes();
			Object name = attributes.getAttribute(StyleConstants.NameAttribute);
			
	
			if ((name instanceof HTML.Tag) && (name == HTML.Tag.TABLE)) {
				// Build up content text as it may be within multiple elements
				//StringBuffer text = new StringBuffer();
				int count = element.getElementCount();
				

				
				if(count > 100) {
					System.out.println("-->" + count + "<--");
					for (int i = 0; i < count; i++) {
						Element tr = element.getElement(i);
						parseTr(tr);
					}
				}
	
			}
		}
		System.exit(0);
	}

	private boolean exists(String code) {
		boolean ret = false;
		try {
			Statement s = connect.createStatement();
			ResultSet r = s.executeQuery("select * from items where code=\"" + code + "\"");
			
			r.last();
			int rowCount = r.getRow();
			r.close();
			s.close();
			ret = rowCount > 0;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(ret);
		return ret;
	}
	
	private void update(String code, String qty) {
		try {
			if (insertSQL) connect.createStatement().executeUpdate("update items set quantity=\"" + qty + "\" where code=\"" + code + "\"");
			System.out.println("update items set quantity=\"" + qty + "\" where code=\"" + code + "\";");
		} catch (Exception e) {
			System.out.println("Did not run query: update items set quantity=\"" + qty + "\" where code=\"" + code + "\";");
			e.printStackTrace();
		}
		
	}
	
	
	private void parseTr(Element tr) throws BadLocationException {
		
		int count = tr.getElementCount();
		//System.out.println(count);
		//if(count == 5) {
			String data0 = "";
			String data1 = "";
			for (int i = 0; i < count; i++) {
				Element td = tr.getElement(i);
				String data = parseTd(td, i);
				//System.out.println("Data: " + data);

				if(i == 0) data0 = data;
				if(i == 1) data1 = data;
				
				
				//	//System.out.println("NewName: " + newName + " Data" + i + ": " + data);

				//System.out.println("I: " + i + " Data:" + data + ":");
			}
			//counter++;
			if(data0.startsWith("AL")) {
				//if(counter >= 3000 && counter < 4000) {
					if(data1.equals("") || data1.equals("-1")) data1 = "0";
					if(exists(data0)) {
						update(data0, data1);
					}
				//}
			}
			
			
			//if(!data1.equals("") && !data3.equals("0") && !data3.equals("")) {
			//	if(newName.equals(data1)) {
			//		System.out.println("update table set price=" + data3 + " where name = \"" + newName + "\"");
			//	} else {
			//		System.out.println("update table set price=" + data3 + " where name = \"" + newName + " " + data1 + "\"");
			//	}
			//}
			//System.out.println();
		//}
		
	}


	private String parseTd(Element td, int index) throws BadLocationException {
		//System.out.println(td.getElementCount());
		
		String data = "";
		AttributeSet attributes = td.getAttributes();
		Object name = attributes.getAttribute(StyleConstants.NameAttribute);
		
		if ((name instanceof HTML.Tag) && (name == HTML.Tag.TD)) {
			if(td.getElement(0).getName().equals("p-implied")) {
				data = parseTd2(td.getElement(0));
			}
		}
		return data;
	}


	private String parseTd2(Element element) throws BadLocationException {

		String ret1 = "";
		//System.out.println("Count: " + element.getElementCount());
		for(int i = 0; i < element.getElementCount(); i++) {
			if(element.getElement(i).getName().equals("content") && (i == 0 || i == 2)) {
				if(i == 0) {
					ret1 = parseContent(element.getElement(i));
					ret1 = ret1.replaceAll(",", "");
					if(ret1.length() == 1) {
						ret1 = ret1.replaceAll("\\W*", "");
					}
					//System.out.println("Ret12: ->" + ret1 + "<-");
				}
				if(i == 2) {
					String temp = parseContent(element.getElement(i));
					temp = temp.replaceAll("\\W*", "");
					if(!temp.equals("")) {
						ret1 += " " + temp;
					}
					//System.out.println("Ret13: " + ret1);
				}
			}
		}
		//System.out.println("Ret14: ->" + ret1 + "<-");
		return ret1;
	}

	private String parseContent(Element element) throws BadLocationException {
		String line = "";
		if(element.isLeaf()) {
			if (element.getAttributes().getAttribute(StyleConstants.NameAttribute) == HTML.Tag.CONTENT) {
				int startOffset = element.getStartOffset();
				int endOffset = element.getEndOffset();
				int length = endOffset - startOffset;
				line = htmlDoc.getText(startOffset, length);
			}
		}
		return line;
	}
	

	/*
	private String parseTable(Element element) throws BadLocationException {
		//System.out.println("// Parse Table");
		Element content = element.getElement(0).getElement(0).getElement(0).getElement(0);
		//System.out.println(content.getName());
		return parseContent(content);
		
	}
	*/
}
