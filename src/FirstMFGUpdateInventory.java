import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class FirstMFGUpdateInventory {
	
	
	
	
	
	private Connection connect = null;
	
	
	public static void main(String args[]) throws Exception {
		new FirstMFGUpdateInventory();
	}
	
	public FirstMFGUpdateInventory() throws IOException {
		
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String connectString = "jdbc:mysql://mysqlhost/substruct?user=root&password=";
			connect = DriverManager.getConnection(connectString);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		
		BufferedReader read = new BufferedReader(new FileReader("test.txt"));
		String line = "";
		while((line = read.readLine()) != null) {
			String array[] = line.split("\t");
			
			if(exists(array[4])) {
			
				System.out.println("Exists: " + exists(array[4]));
				
				for(int i = 0; i < array.length; i++) {
					System.out.println("I: " + i + " Value: " + array[i]);
				}
			}
			
		}
		
	}
	
	
	private boolean exists(String code) {
		boolean ret = false;
		try {
			Statement s = connect.createStatement();
			ResultSet r = s.executeQuery("select * from items where code like \"%" + code + "%\"");
			
			r.last();
			int rowCount = r.getRow();
			r.close();
			s.close();
			ret = rowCount > 0;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(ret);
		return ret;
	}
}
