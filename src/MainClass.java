import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;

public class MainClass {
	
	public static void main(String args[]) throws Exception {
		HTMLParse hp = new HTMLParse();
		new ParserDelegator().parse(new FileReader("sitemap.jsp"), hp, true);
	}
}

class HTMLParse extends HTMLEditorKit.ParserCallback {
	
	int tabs = -1;

	FileOutputStream fos = null;
	PrintWriter out = null;
	
	
	public HTMLParse() {
		try {
			fos =  new FileOutputStream(new File("output.html"));
			out = new PrintWriter(fos);
			//out = new BufferedOutputStream(new FileOutputStream(new File("output.html")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void handleError(String error, int pos) {
		System.out.println("Error: " + error);
		//System.exit(0);
	}
	
	public void handleComment(char[] data, int pos) {
		System.out.println("handleComment:");
		tabs++;
		for(int i = 0; i < tabs; i++) out.print("\t");
		out.print("<!--");
		for(int i = 0; i < data.length; i++) out.print(data[i]);
		out.print("-->");
		out.println();
		tabs--;
	}
	
	public void handleText(char[] data, int pos) {
		System.out.println("handleText:");
		tabs++;
		for(int i = 0; i < tabs; i++) out.print("\t");
		for(int i = 0; i < data.length; i++) out.print(data[i]);
		out.println();
		tabs--;
	}

	public void handleStartTag(HTML.Tag t, MutableAttributeSet a, int pos) {
		System.out.println("handleStartTag:");
		tabs++;
		for(int i = 0; i < tabs; i++) out.print("\t");
		out.println("<" + t.toString() + " " + a + ">");
	}

	public void handleSimpleTag(HTML.Tag t, MutableAttributeSet a, int pos) {
		System.out.println("handleSimpleTag:");
		tabs++;
		for(int i = 0; i < tabs; i++) out.print("\t");
		out.println("<" + t.toString() + " " + a + ">");
		tabs--;
	}

	public void handleEndTag(HTML.Tag t, int pos) {
		System.out.println("handleEndTag:");
		for(int i = 0; i < tabs; i++) out.print("\t");
		out.println("</" + t.toString() + ">");
		tabs--;
	}

}
